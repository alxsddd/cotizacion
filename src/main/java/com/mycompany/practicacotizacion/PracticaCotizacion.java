/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.practicacotizacion;

/**
 *
 * @author Alexis
 */
public class PracticaCotizacion {

  public static void main(String[] args) {
        // TODO code application logic here
        Cotizacion cotizacion = new Cotizacion();
        
        System.out.println("Numero de la cotizacion:"+ cotizacion.getNumCotizacion());
        System.out.println("Descripcion:"+ cotizacion.getDescripcion());
        System.out.println("Precio:"+ cotizacion.getPrecio());
        System.out.println("Pago inicial:"+ cotizacion.getPagoInicial());
        System.out.println("Plazo:"+ cotizacion.getPlazo());
        System.out.println("----------------------------------------------------");
        System.out.println("el pago inicial es de:"+ cotizacion.calcularPagoInicial());
        System.out.println("el total a financiar es de:"+ cotizacion.calcularFin());
        System.out.println("el pago mensual es de:"+ cotizacion.pagoMensual());
        
       
        
    }
    
}